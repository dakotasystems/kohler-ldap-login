xquery version "3.0";

module namespace datamart="datamart/namespace";
import module namespace l="l" at "./ldap.xql";
declare namespace sm="http://exist-db.org/xquery/securitymanager";
declare variable $datamart:admin := "admin"; 
declare variable $datamart:password := "admin"; 
 
 declare function datamart:processLogin($user as xs:string, $password as xs:string, $app as xs:string, $url as xs:string){
	let $sessionRole := if (system:as-user($datamart:admin, $datamart:password, sm:user-exists($user))) then (
		let $ldapResult := l:ldapCheck($user, $password)
		return if ($ldapResult = 'Viewer') then (
    		let $groups := system:as-user('admin','admin', sm:get-user-groups($user))
        	let $roles := if (count($groups) > 1) then (
        		for $group in $groups
        		return concat($group, ',')
        	) else (
        	    if (count($groups) = 1) then ($groups) 
        	    else ("Viewer")
        	)
			return substring(string-join($roles),0,string-length(string-join($roles))) (: Remove last comma :)
	    ) else (
	        $ldapResult
        ) 
    ) else (
        let $createAccount := system:as-user($datamart:admin, $datamart:password, sm:create-account($user, $password, "Viewer", ''))
        return if ($app = "trademark") then ("_redirect_login") else ("Viewer")
    )
    return if (not(contains($sessionRole, "_"))) then (
		let $sessionCreate := session:create(),
        	$sessionTimeCreated := current-dateTime(),
        	$expires := datetime:datetime-range($sessionTimeCreated,xdt:dayTimeDuration("PT1H"), 2),
        	$sessionExpiration := session:set-attribute('expires',$expires[last()]),
        	$sessionUser := session:set-attribute('user',$user),
			$sessionID := session:get-id(),
			$setSessionID := session:set-attribute('sessionID',$sessionID)
		return concat($url, '|', $sessionID, '|', $sessionRole)
	) else (
		$sessionRole
	)
 };
