xquery version "3.0";
module namespace l="l";
import module namespace tsh="tsh" at "./global.xql";
import module namespace xmldb="http://exist-db.org/xquery/xmldb";
declare namespace dsml="http://www.dsml.org/DSML";
declare namespace jndi="http://exist-db.org/xquery/jndi";
declare namespace util="http://exist-db.org/xquery/util";

declare function l:ldapCheck($user,$password) {
    let $LDAP-URL := "ldap://ldap.kohlerco.com:389"
    let $ENTRY-ROOT-CONTEXT := "OU=Enterprise,DC=kohlerco,DC=com"
    
    let $contextProps :=  
        <properties>  
        	<property name="java.naming.factory.initial" value="com.sun.jndi.ldap.LdapCtxFactory"/>  
        	<property name="java.naming.provider.url" value="{$LDAP-URL}"/>  
        	<property name="java.naming.security.authentication" value="simple"/>  
        	<property name="java.naming.security.credentials" value="{$password}"/>  
        </properties> 
    
    let $ctx := try {
    	jndi:get-dir-context($contextProps)
    } catch * {
      	util:log-system-out(concat($err:code, ": ", $err:description))
    }
    
    let $searchAttrs := concat('(CN=', $user, ')')
    
    let $entry := jndi:search($ctx, $ENTRY-ROOT-CONTEXT, $searchAttrs, 'subtree')
    
    let $userDN := $entry//dsml:entry//dsml:attr[@name = 'distinguishedName']/dsml:value
    
    let $close := if(exists($ctx)) then jndi:close-context($ctx) else ()
    
    let $contextProps := if ($userDN != '') then (
        <properties>  
        	<property name="java.naming.factory.initial" value="com.sun.jndi.ldap.LdapCtxFactory"/>  
        	<property name="java.naming.provider.url" value="{$LDAP-URL}"/>  
        	<property name="java.naming.security.authentication" value="simple"/>  
        	<property name="java.naming.security.principal"  value="{$userDN}"/>
        	<property name="java.naming.security.credentials" value="{$password}"/>  
        </properties> 
    ) else () 
    
    let $ctx := try {
    	jndi:get-dir-context($contextProps)
    } catch * {
      	util:log-system-out(concat($err:code, ": ", $err:description))
    }
    
    let $result := if(exists($ctx)) then ("Viewer") else ("_ldap_failed")
    
    let $close := if(exists($ctx)) then jndi:close-context($ctx) else ()
    
    return $result
};