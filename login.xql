xquery version "3.0";

declare namespace request="http://exist-db.org/xquery/request";
import module namespace datamart="datamart/namespace" at "login.xqm";
import module namespace tsh="tsh" at "./global.xql";

let $user := request:get-parameter("user","")
let $password := request:get-parameter("password", '')
let $app := request:get-parameter("app", '')
let $url := request:get-parameter("url", concat('http://', $tsh:server, ':8280/exist/datamart/index.html'))
    let $url := if (contains($url, "?")) then
            substring-before($url, "?")
        else
            $url
return datamart:processLogin(lower-case($user), $password, $app, $url)
