function checkKey(event) {
    if (event.which == 13 || event.keyCode == 13) {
        login();
    }
}

function login() {
    var user = $("#user").val();
    var pwd = $("#pwd").val();
    var longApp = document.referrer.substring(document.referrer.indexOf("apps/")+5);
    var app = longApp.substring(0,longApp.indexOf("/"));
    if(user == "" || user == null || pwd == "" || pwd == null) {
		$("#loginError").text("Please enter a valid username and password");		
	} else {
        var logSuccess = false;
    	var url = "";
        $.ajax({
            dataType: "text",
            type: "POST",
            url: "login.xql",
            data:{user: user, password: pwd, app: app},
            cache: false,
            error: function(data) {
                alert("This link is broken or the resource is unavailable.");
            },
            success: function(data) {
                if(data.substring(0,1) != '_') {
                    $("#loginError").text("");
                    var dataArr = data.split("|");
                    url = dataArr[0];
                    setSession('sessionNum',dataArr[1],1);
                    setSession('role',dataArr[2],1);
                    setSession('user',user,1);
                    logSuccess = true;
                } else {
                    var error;
                    if (data == "_ldap_failed") {
                        logSuccess = false;
                        error = "*LDAP authentication failed for user " + user;
                    } else if (data == "_redirect_login") {
                    location.href = url;
                    }
                $("#loginError").text(error);
                }
            }
        }).done(function() {
            if(logSuccess === true) {
                if (document.referrer != "" && document.referrer.indexOf("login") == -1) {
                    location.href = document.referrer;
                } else {
                    location.href = url;
                }
            }
        });
    }
}

function getSession(sName) {
	var i,x,y;
	var sessions = document.cookie.split(";");
	for (i=0;i < sessions.length;i++) {
		x = sessions[i].substr(0,sessions[i].indexOf("="));
		y = sessions[i].substr(sessions[i].indexOf("=")+1);
		x = x.replace(/^\s+|\s+$/g,"");
		if (x ==sName) {
			return unescape(y);
		}
	}
}

function setSession(sName,val,exdays) {
	var exdate=new Date();
	exdate.setDate(exdate.getDate() + exdays);
	var sVal=escape(val) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
	document.cookie=sName + "=" + sVal + ";path=/";
}

function checkSession(onload) {	
	var session = getSession("sessionNum");
	//TODO: check expiration of cookie
	//TODO: check that session still exists
	
	if(session == null || session == "") {
		return false;
	} else {
		return true;
	}
}

if (!Date.prototype.toISOString) {
	(function() {
		function pad(number) {
			var r = String(number);
			if ( r.length === 1 ) {
				r = '0' + r;
			}
				return r;
			}

		Date.prototype.toISOString = function() {
			return this.getUTCFullYear() + '-' + pad(this.getUTCMonth() + 1) + '-' + pad(this.getUTCDate()) + 'T' + pad(this.getUTCHours()) + ':' + 
			pad(this.getUTCMinutes()) + ':' + pad(this.getUTCSeconds()) + '.' + 
			String((this.getUTCMilliseconds()/1000).toFixed(3)).slice(2, 5) + 'Z';
		};

	}());
}