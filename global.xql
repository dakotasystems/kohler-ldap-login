module namespace tsh="tsh";
declare namespace dsml="http://www.dsml.org/DSML";
declare namespace tview="http://www.terraxml.com/tview";
declare namespace request="http://exist-db.org/xquery/request";
declare namespace util="http://exist-db.org/xquery/util";
import module namespace xmldb="http://exist-db.org/xquery/xmldb";
import module namespace l="l" at "./ldap.xql";

(: Modify the following per the server running them :)
(: declare variable $tsh:server := 'localhost'; :)
(: declare variable $tsh:server := request:get-hostname(); :)
declare variable $tsh:server := 'uswix160';
declare variable $tsh:wc := 'http://wc-dev.kohlerco.com/Windchill/';
declare variable $tsh:wcUser := 'wcadmin';
declare variable $tsh:wcPwd := 'wcadmin9';
declare variable $tsh:wcTransfer := '/techpubs/datamart/wc-dev/';
declare variable $tsh:users := '/db/tsh/users.xml';
declare variable $tsh:content := '/db/TerraView/Content/KohlerDatamart';
declare variable $tsh:mconfig := system:as-user("admin", "admin", doc('/db/TerraView/System/MasterConfig.xml'));
declare variable $tsh:kohlerImageLink := 'http://www.ca.kohler.com/onlinecatalog/jpg600/';
declare variable $tsh:sterlingImageLink := 'http://www.sterlingplumbing.com/onlinecatalog/jpg600/';
declare variable $tsh:rootVarHref := concat('http://', $tsh:server, '.kohlerco.com:8084/TerraViewWeb/ContentFetcher?contentId=/TerraView/Content/KohlerDatamart');
declare variable $tsh:export := 'C:\apps\export';
declare variable $tsh:imports := 'C:\apps\imports\';
declare variable $tsh:admin := 'admin';
declare variable $tsh:password := 'admin';
declare variable $tsh:logs := '/db/tmp/';

(: These do not need to be modified :)
declare variable $tsh:xquery := concat('http://', $tsh:server, ':8280/exist/datamart/');
declare variable $tsh:php := concat('http://', $tsh:server, ':8084/datamart/');

(:CUSTOM FUNCTIONS:)

declare function tsh:secure($role)
{
    let $roles := sm:get-user-groups(xmldb:get-current-user())
    return
        if(xmldb:get-current-user()='guest') then
            response:redirect-to(xs:anyURI(concat('http://', $tsh:server, ':8280/exist/datamart/login.xql?url=', session:encode-url(request:get-uri()))))
        else if(not($roles=$role or xmldb:get-current-user()='admin' )) then
            response:redirect-to(xs:anyURI(concat('http://', $tsh:server, ':8280/exist/datamart/login.xql?attempt=blocked&amp;url=', session:encode-url(request:get-uri()))))        
        else ()
};


declare function tsh:cleanupAll($sku)
{
    let $login := xmldb:login("/db/TerraView", "admin", "admin")
    let $collection := collection("/db/TerraView/Content/")
    
for $file in $collection
    let $path := substring-before($file/base-uri(), string($file/pub/tview:properties/tview:node-id))
    let $doc := string($file/pub/tview:properties/tview:node-id)

where $file//root-var/@prod_id=$sku and not(contains($file/base-uri(), 'KohlerDatamart/1'))

return
    xmldb:remove($path, $doc)
    
};


declare function tsh:createFile($url, $file, $content)
{
let $login := xmldb:login("/db", "admin", "admin")
let $path := substring-after($url, 'db/')
let $cleanpath := concat('/db/', $path)
let $fullpath := if (substring($cleanpath, string-length($cleanpath), 1) = '/') then
    $cleanpath
    else
        concat($cleanpath, '/')

let $collectionCheck :=    
    if(collection($fullpath)) then
        ()
    else
        xmldb:create-collection("/db", $path)
    
let $fileCheck :=
    if(doc(concat($fullpath, $file))) then
        ()
    else
        xmldb:store($fullpath, $file, $content)
    return 'a'
};

declare function tsh:login($type) {
    let $user := session:get-attribute("user")
    let $password := session:get-attribute("password")
    return
    if($type = 'admin') then
            (xmldb:login("/db", 'admin', 'admin'))
        else
            (xmldb:login("/db", $user, $password))
};

declare function tsh:log($file, $message) {
    let $output := concat($tsh:logs, $file)
    (:let $m := <message date="{current-dateTime()}">{$message}</message>:)
    return file:serialize($message, $output, ("method=text", "media-type=text/plain"), true())
};

declare function tsh:dateFix($date) {
    if($date='') then
            ()
        else
    let $good := try {xs:date($date)}
                            catch *
                                {'bad'}
    return
    if (string($good) != 'bad') then
        format-date(xs:date($date), '[Y0001]-[M01]-[D01]')
    else if ($date = '') then ''
    else
        let $d := tokenize($date, '/| |-')
        let $yl := string-length($d[3])
        let $year := if($yl=4) then $d[3] else $d[1]
        
        let $month := if($yl=4) then
            if (xs:integer($d[1]) lt 10) then
                concat('0', number($d[1])) 
            else $d[1]
        else if (xs:integer($d[2]) lt 10) then
            concat('0', number($d[2]))
            else $d[2] 
        
        let $day := if($yl=4) then
            if (xs:integer($d[2]) lt 10) then
                concat('0', number($d[2]))
            else $d[2]
        else if (xs:integer($d[3]) lt 10) then
            concat('0', number($d[3]))
            else $d[3]
            
        return 
        xs:date(concat($year, '-', $month, '-', $day))
};